package DAO;


import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.content.Context;

public class DAOSession extends SQLiteOpenHelper
{
  private static final String DELETE_SESION="DROP TABLE IF EXISTS SESION_LOCAL";
  public static final String TABL_NAME="SESION_LOCAL";

  private static final String CREATE_SESION="CREATE TABLE SESION_LOCAL ( USEID TEX, USERTOKEN TEXT ) ";

  private  static final String dbNAME="internalst.db";

  public DAOSession(Context cont)
  {
    super(cont,dbNAME,null,1);
  }










  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase)
  {
    try
    {
      sqLiteDatabase.execSQL(CREATE_SESION);
    }catch (Exception ex)
    {
      Log.wtf("no se peude crear la base de datos",ex);
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
  {
    try
    {
      sqLiteDatabase.execSQL(DELETE_SESION);
      this.onCreate(sqLiteDatabase);
    }catch (Exception ex)
    {
      Log.wtf("LIMPIANDO LA SESION",ex);
    }
  }
}
