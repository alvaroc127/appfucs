// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = 
{
  production: false,
  baseurl:'http://localhost:8080/APIBackFucs/',
  adminsendpoints: 
  {
  	activeuser:'admin/activate',
  	registrausuario: 'usuario/registro',
  	unactive:'admin/unactive',
  	tipodoc:'usuario/tipodocs',
    changepass:'admin/changpass',
  	roles:'usuario/roles'
  },
  medicalendpoints:
  {
  		bindusermon: 'medical/bindusermon',
  		unbindusermon: 'medical/unbindusermon',
  		findmon: 'medical/findmon',
  		activemov: 'medical/activemovil',  	
  		unactivemov: 'medical/unactivemovil',
  		activealert: 'medical/activealert',
  		unactivealer: 'medical/unactivealert',
  		validateactivemovil:'medical/validateactivealertmovil',
  		consultsignal: 'medical/consultsignal',
      consultusernomon:'medical/usernomon',
      consultusermon:'medical/usermon',
      consultalert:'medical/consultalrms'
  },
  loginenpoint: 'login/user',
  logout:'login/logout',
  cifredkey: 'F0C5C1FR3DK3Y123',
  topicsubs:'fucstopic'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
