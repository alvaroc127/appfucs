import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {loginModule} from './login/login.module';
import {HttpClientModule} from '@angular/common/http';
import {AdminDashBoardModule} from './admin/dashboard/admin.dashboard.module';
import {dashboarModule} from './medical/dashboard/dashboard.module';



const routes: Routes = 
[
	{path:"login",loadChildren:'./login/login.module#loginModule'},
	{path: "**", redirectTo:"/login"}	
];

@NgModule({
  imports: [HttpClientModule,RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [] 
})
export class AppRoutingModule { }
