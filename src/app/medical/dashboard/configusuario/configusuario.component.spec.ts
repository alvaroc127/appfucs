import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigusuarioComponent } from './configusuario.component';

describe('ConfigusuarioComponent', () => {
  let component: ConfigusuarioComponent;
  let fixture: ComponentFixture<ConfigusuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigusuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigusuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
