import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {Rol} from '../../../model/rol.model';
import {Permiso} from '../../../model/permiso.model';
import {Usuario} from '../../../model/usuario.model';
import {Monitor} from '../../../model/monitor.model';
import {AuthservicesService} from  '../../../services/authservices.service';
import {MonitorconfigService} from '../../../services/monitorconfig.service';


@Component({
  selector: 'app-configusuario',
  templateUrl: './configusuario.component.html',
  styleUrls: ['./configusuario.component.css']
})
export class ConfigusuarioComponent implements OnInit {
	public sourcedash:number = 1;

  public users:Usuario[];

  public monitores:Monitor[];

 
   userselect:Usuario;

   userbindmon=
   {
     ipMonitor:'',
     username:''
   };

  constructor(private auth:AuthservicesService,
    private router:Router,
    private mont:MonitorconfigService) 
  {
    this.users=new Array();
    this.monitores=new Array();

    let per =new Permiso(1,'Permiso test');
    let permi=new Array();
    permi.push(per);
    let rol=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol test'
      });
     this.userselect= new Usuario({ 
         arraySingal:new Array(),
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
     this.userselect.username='prueba nobmre';
  }

  public getUsername():string 
  {
  	try
     {
       this.auth.getUserSession();
       return this.auth.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }
       sessionStorage.clear();
       return String(e);
     }
  }


   /* public getUserWithoutRol(): Usuario[]
  {
    let per =new Permiso(1,'Permiso test');
    let permi=new Array();
    permi.push(per);
    let rol=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol test'
      });
     let uss= new Usuario({ 
         arraySingal:new Array(),
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
     uss.name='prueba nobmre';
     uss.numerodoc='1010230671';
     uss.username='11010230671';
     uss.tipodoc=this.getTypeDoc(uss.username.charAt(0)); 
     let arr =new Array();


    let rol1=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol test2'
      });
     let uss1= new Usuario({ 
         arraySingal:new Array(),
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
     uss1.name='prueba 2 ';
     uss1.numerodoc='51986066';
     uss1.username='251986066';
     uss1.tipodoc=this.getTypeDoc(uss1.username.charAt(0)); 
     arr.push(uss);
     arr.push(uss1);
     return arr;
  }*/


  public setUserSelect(userselct:string)
  {
    for(const us in this.users)
    {
      if(this.users[us].username ==userselct)
      {
        this.userselect.numerodoc=this.users[us].numerodoc;
        this.userselect.username=this.users[us].username;
        this.userselect.tipodoc=this.users[us].tipodoc;
        break;
      }
    }
     this.userselect.tipodoc=this.getTypeDoc(this.userselect.username.charAt(0)); 
  }

   public sendForm(ipmonitor:string)
   {
    if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.router.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.router.navigate(['/login']);
      return;
    }
    this.userbindmon.username=this.userselect.username;
    this.userbindmon.ipMonitor=ipmonitor;
    let token=this.auth.getBarerToken();
    console.log(this.userbindmon);
    this.mont.bindUserMont(this.userbindmon,token).subscribe(
      response=>
      {
        if(response.status='00')
        {
          alert('usuario y monitor vinculador de forma correcta ');
          this.cleanUserandMonitor();
          window.location.reload();
        }
      },
      error=>
      {
        alert(' no se pudo vincular el monitor y el usuraio');
        console.log('se genero un error ',error);
      }
      
    );
    this.resetUserSelect();



   }


   public getTypeDoc(tipdoc:string):any
  {
    let array=
    [
      { id: '1', descrip:'cédula' },
      { id: '2', descrip:'cédula extranjería' },
      { id: '3', descrip:'tarjeta identidad' }
    ]
    return  array.filter( (tip:any) => { return tip.id == tipdoc})[0];
  }


 /* public getMonitoreNoVinculados():Monitor[]
  {
    let arrMon=new Array();
    let mon1=new Monitor('192.168.1.12',1);
    let mon2=new Monitor('192.168.1.13',2);
    let mon3=new Monitor('192.168.1.15',3);
    arrMon.push(mon1);
    arrMon.push(mon2);
    arrMon.push(mon3);
    return arrMon;
  }*/

  ngOnInit(): void 
  {
      if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.router.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.router.navigate(['/login']);
      return;
    }
    let token=this.auth.getBarerToken();
    this.mont.findUserNoMonitor(token).subscribe(
      response=>
      {
        if(response.status=='00')
        {
          let interol=
          {descrtip:'',id:10,permiso:new Array()};

          let usuarios=response.usuarios;
          for( let ind in usuarios)
          {
            let usser=usuarios[ind];
            let roles=usser.roles;
            for(let rol in roles)
            {
              let perm=new Permiso(1,'otro');
              let arrperm=new Array();
              arrperm.push(perm);
              interol=
                {
                descrtip:roles[rol].descrip,
                id:parseInt(rol,10),
                permiso:arrperm
                };
            }
            let role =new Rol(interol);
            let intus=
            { 
           arraySingal:new Array(),
           rol:role,
           name:'',
           numerodoc:usser.numerodoc,
           tipodoc:{id:usser.tipoDoc.idtipdoc,descrip:usser.tipoDoc.descrip},
           mail:'',
           username:usser.tipoDoc.idtipdoc+usser.numerodoc
           };
           let userr=new Usuario(intus);
           this.users.push(userr);
         }
        }
      },
      error=>
      {
        console.log('error en la consulta ',error);
      }
      );
      this.mont.findMonitorNoBind(token).subscribe(
        response=>
        {
          if(response.status=='00')
          {
            let monitrs=response.map;
            for(const m in monitrs)
            {
              let mmonit=new Monitor(m,parseInt(monitrs[m],10));
              this.monitores.push(mmonit);
            }

          }

        },error=>
        {
          console.log('error en la consulta de monitor',error);
        });
  }



  public resetUserSelect()
  {
    this.userselect.username='';
    this.userselect.numerodoc='';
    this.userselect.rol=new Rol({
      descrtip:'',
      id:0,
      permiso:new Array()
    });
    this.userbindmon.username='';
    this.userbindmon.ipMonitor='';
  }


  public cleanUserandMonitor()
  {
    this.monitores=this.monitores.filter((mon:Monitor)=>
    {
      return mon.getip !== this.userbindmon.ipMonitor;
    });
    this.users=this.users.filter((user:Usuario)=>{
      return user.username !== this.userbindmon.username;
    });
  }




}