import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomconsultComponent } from './customconsult.component';

describe('CustomconsultComponent', () => {
  let component: CustomconsultComponent;
  let fixture: ComponentFixture<CustomconsultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomconsultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomconsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
