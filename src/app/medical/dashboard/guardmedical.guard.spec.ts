import { TestBed } from '@angular/core/testing';

import { GuardmedicalGuard } from './guardmedical.guard';

describe('GuardmedicalGuard', () => {
  let guard: GuardmedicalGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(GuardmedicalGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
