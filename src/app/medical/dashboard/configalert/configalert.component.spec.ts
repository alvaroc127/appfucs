import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigalertComponent } from './configalert.component';

describe('ConfigalertComponent', () => {
  let component: ConfigalertComponent;
  let fixture: ComponentFixture<ConfigalertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigalertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigalertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
