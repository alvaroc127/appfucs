import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {AuthservicesService} from '../../../services/authservices.service';
import {DispositivService} from '../../../services/dispositiv.service';
import {AlertconfigService} from '../../../services/alertconfig.service';



@Component({
  selector: 'app-configalert',
  templateUrl: './configalert.component.html',
  styleUrls: ['./configalert.component.css']
})
export class ConfigalertComponent implements OnInit 
{

public sourcedash:number = 1;
public eventMovil=new Subject<string>();



  constructor(private router:Router,
   private auth:AuthservicesService,
    private disser:DispositivService,
    private alertcfg:AlertconfigService) 
  {
    this.disser.getInfoDevice();
  }


  activealert=
  {
    username:''
  }

  formacti='';

  public getUsername():string 
  {
  	 try
     {
       this.auth.getUserSession();
       return this.auth.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }
       sessionStorage.clear();
       return String(e);
     }
  }

  ngOnInit(): void {
    this.eventMovil.subscribe(
      event=>
      {
        if(event=='movilok')
        {
          let token=this.auth.getBarerToken();
          this.alertcfg.activeAlert(this.activealert,token).subscribe(
            response=>
            {
              alert('se activo la alerta con exito');
              let user
               {
                 username:this.auth.getUsername();
               }
               let token=this.auth.getBarerToken();
               this.alertcfg.isActiveAlert(user,token);
            },error=>
            {
              alert('la alerta no pudo ser activada');
              console.log('error de alerta',error);
            }
            );
        }
        if(event=='movilun')
        {
            let token=this.auth.getBarerToken();
            this.alertcfg.unactiveAlert(this.activealert,token).subscribe(
            response=>
            {
              alert('se desactivo la alerta con exito');
            },error=>
            {
              alert('la alerta no pudo desactivar');
              console.log('error de alerta',error);
            }
            );
        }
      },error=>
      {
        console.log('error en activar el movil',error);
      }
     );
  }


  public sendChangeAlert(formact:string)
  {
     if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.router.navigate(['/login']);
      return;
    }
    try
    {
    this.activealert.username=this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.router.navigate(['/login']);
      return;
    }
    /*if(false==this.disser.isMobile)
    {
      alert(' usted no esta conectado desde un dispostivo movil ');
      return;
    }*/
    if(this.formacti=='1')
    {
      let token=this.auth.getBarerToken();
      console.log('se enviara ',this.activealert);
      this.alertcfg.activeMovil(this.activealert,token).subscribe(
        response=>
        {
          if(response.status=='00')
          {
            //alert('se activo el movil se procede a activar las alertas');
            this.eventMovil.next('movilok');
          }
        },
        error=>
         {
          alert('no se pudo activar la opcion movil');
          console.log('error en error',error);
         }
        )
    }
    if(this.formacti=='0')
    {
      let token=this.auth.getBarerToken();
      console.log('se enviara ',this.activealert);
      this.alertcfg.unactiveMovil(this.activealert,token).subscribe(
        response=>
        {
          if(response.status=='00')
          {
            //alert('se desactivo el movil, se desactivaran las alertas');
            this.eventMovil.next('movilun');
          }
        },
        error=>
         {
          alert('no se pudo desactivar el movil');
          console.log('error en error',error);
         }
        )
    }




  }




}
