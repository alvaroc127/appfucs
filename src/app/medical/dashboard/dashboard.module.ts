import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {dashboard} from './dashboard.component';
import {modelmodule} from '../../model/model.module';
import {navBarModule} from '../../navbar/dashboard.navbar.module';
import {GuardmedicalGuard} from './guardmedical.guard';
import { RouterModule, Routes } from '@angular/router';
import { ConfigalertComponent } from './configalert/configalert.component';
import { ConfigusuarioComponent } from './configusuario/configusuario.component';
import { GraficasComponent } from './graficas/graficas.component';
import { CustomconsultComponent } from './customconsult/customconsult.component';
import { DesvincularusumonComponent } from './desvinculausumon/desvincularusumon.component';
import { ChartsModule } from 'ng2-charts';

const routes:Routes =
[
	{path:"medicaldashboard", component:dashboard, canActivate:[GuardmedicalGuard]},
	{path:"configusu", component:ConfigusuarioComponent,canActivate:[GuardmedicalGuard]},
	{path:"configalert", component:ConfigalertComponent,canActivate:[GuardmedicalGuard]},
	{path:"consultper", component:GraficasComponent,canActivate:[GuardmedicalGuard]},
	{path:"devincularusumo", component:DesvincularusumonComponent,canActivate:[GuardmedicalGuard]}
];

@NgModule({
	imports:[navBarModule,modelmodule,BrowserModule,FormsModule,ChartsModule,RouterModule.forRoot(routes)],
	declarations:[dashboard, ConfigalertComponent, ConfigusuarioComponent, GraficasComponent, CustomconsultComponent, DesvincularusumonComponent],
	providers:[GuardmedicalGuard],
	exports:[RouterModule,dashboard]
})
export class dashboarModule{


}