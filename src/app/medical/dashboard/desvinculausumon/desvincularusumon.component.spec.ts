import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesvincularusumonComponent } from './desvincularusumon.component';

describe('DesvincularusumonComponent', () => {
  let component: DesvincularusumonComponent;
  let fixture: ComponentFixture<DesvincularusumonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesvincularusumonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesvincularusumonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
