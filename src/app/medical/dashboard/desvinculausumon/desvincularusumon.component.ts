import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {Usuario,InterfaceUsuario} from '../../../model/usuario.model';
import {MonitorconfigService} from '../../../services/monitorconfig.service';
import {AuthservicesService} from '../../../services/authservices.service';
import {Rol} from '../../../model/rol.model';
import {Permiso} from '../../../model/permiso.model';


@Component({
  selector: 'app-desvincularusumon',
  templateUrl: './desvincularusumon.component.html',
  styleUrls: ['./desvincularusumon.component.css']
})
export class DesvincularusumonComponent implements OnInit {

  public userselect:InterfaceUsuario;
  public sourchedash:number=1;
  public usuariobindmon:Usuario[];



  constructor(private auth:AuthservicesService,
          private monconfi:MonitorconfigService,
          private rout:Router) 
  {
    this.usuariobindmon=new Array();
  	let per =new Permiso(1,'Permiso test');
  	let permi=new Array();
  	permi.push(per);
  	let rol=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol'
      });
    
  	this.userselect= new Usuario({ 
         arraySingal:[],
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
    
  }

  userbindmon=
   {
     ipMonitor:'',
     username:''
   };

  ngOnInit(): void 
  {
     if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    let token=this.auth.getBarerToken();
    this.monconfi.findUserMont(token).subscribe(
      response=>
      {
        if(response.status=='00')
        {
          let interol=
          {descrtip:'',id:10,permiso:new Array()};

          let usuarios=response.usuarios;
          for( let ind in usuarios)
          {
            let usser=usuarios[ind];
            let roles=usser.roles;
            for(let rol in roles)
            {
              let perm=new Permiso(1,'otro');
              let arrperm=new Array();
              arrperm.push(perm);
              interol=
                {
                descrtip:roles[rol].descrip,
                id:parseInt(rol,10),
                permiso:arrperm
                };
            }
            let role =new Rol(interol);
            let intus=
            { 
           arraySingal:new Array(),
           rol:role,
           name:'',
           numerodoc:usser.numerodoc,
           tipodoc:{id:usser.tipoDoc.idtipdoc,descrip:usser.tipoDoc.descrip},
           mail:usser.email,
           username:usser.tipoDoc.idtipdoc+usser.numerodoc
           };
           let userr=new Usuario(intus);
           this.usuariobindmon.push(userr);
         }
        }
      },error=>
      {
        console.log('error en consulta de usurios vinculados',error);
      }
      );
  }


  public quitarRol()
  {
     if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    let token=this.auth.getBarerToken();

    this.monconfi.unBindUserMont(this.userbindmon,token).subscribe(
      result=>
      {
        if(result.status=='00')
        {
        alert('se genero el unbind del monitor');
        this.cleanUserUnBind();
        this.resetUserSelect();
        window.location.reload();
        }
      },error=>
      {
        console.log('algo salio mal',error),
        alert('no se pudo desvincular el usuario');
      }
      );
  }

  public resetUserSelect()
  {
    this.userselect.username='';
    this.userselect.numerodoc='';
    this.userselect.rol=new Rol({
      descrtip:'',
      id:0,
      permiso:new Array()
    });
    this.userbindmon.username='';
    this.userbindmon.ipMonitor='';
  }


  public getUsername():string
  {
    try
     {
       this.auth.getUserSession();
       return this.auth.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }
       sessionStorage.clear();
       return String(e);
     }
  }



  public setUserSelect(userselct:string)
  {
     for(const us in this.usuariobindmon)
    {
      if(this.usuariobindmon[us].username ==userselct)
      {
        this.userselect.numerodoc=this.usuariobindmon[us].numerodoc;
        this.userselect.username=this.usuariobindmon[us].username;
        this.userselect.tipodoc=this.usuariobindmon[us].tipodoc;
        this.userselect.rol=this.usuariobindmon[us].rol;
        this.userselect.mail=this.usuariobindmon[us].mail;
        break;
      }
    }
    this.userbindmon.username=this.userselect.username;
    this.userbindmon.ipMonitor=this.userselect.mail;
     //this.userselect.setTipoDoc();
     this.userselect.tipodoc=this.getTypeDoc(this.userselect.username.charAt(0)); 
  }


 /* public getUserWithRol(): Usuario[]
  {
  	let per =new Permiso(1,'Permiso test');
  	let permi=new Array();
  	permi.push(per);
  	let rol=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol test'
      });
  	 let uss= new Usuario({ 
         arraySingal:new Array(),
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
  	 uss.name='prueba nobmre';
  	 uss.numerodoc='1010230671';
  	 uss.username='11010230671';
     uss.tipodoc=this.getTypeDoc(this.userselect.username.charAt(0)); 
  	 let arr =new Array();


    let rol1=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol test2'
      });
  	 let uss1= new Usuario({ 
         arraySingal:new Array(),
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
  	 uss1.name='prueba 2 ';
  	 uss1.numerodoc='51986066';
  	 uss1.username='251986066';
     uss1.tipodoc=this.getTypeDoc(this.userselect.username.charAt(0)); 
  	 arr.push(uss);
  	 arr.push(uss1);
  	 return arr;
  }*/


  public getTypeDoc(tipdoc:string):any
  {
    let array=
    [
      { id: '1', descrip:'cédula' },
      { id: '2', descrip:'cédula extranjería' },
      { id: '3', descrip:'tarjeta identidad' }
    ]
    return  array.filter( (tip:any) => { return tip.id == tipdoc})[0];
  }

  public cleanUserUnBind()
  {
    this.usuariobindmon=this.usuariobindmon.filter((user:Usuario)=>{
      return user.username !== this.userbindmon.username;
    });
  

  }

}
