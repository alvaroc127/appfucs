import { Injectable } from '@angular/core';
import {Router,CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthservicesService} from '../../services/authservices.service';


@Injectable({
  providedIn: 'root'
})
export class GuardmedicalGuard implements CanActivate {

constructor(private auth:AuthservicesService,private rou:Router)
{

}


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
  {
    let isinsesion=false;
    try
    {
      this.auth.getUserSession();
      if(this.auth.isDoctorNurse() && this.auth.isSesionOn())
      {
        isinsesion=true;
      }else
      {
      	alert(" su sesion vencio o invalid");
      	this.auth.logoutUser();
      	sessionStorage.clear();
      	this.rou.navigate(['/login']);
      }     
    }catch(e)
    {
      if(e == -1)
      {
        isinsesion=false;
        alert(" necesita tener una sesion activa para acceder");
        this.rou.navigate(['/login']);
        //this.auth.logoutUser();
      }
      console.log('algo salio mal',e);
    }
    return isinsesion;
  }
  
}
