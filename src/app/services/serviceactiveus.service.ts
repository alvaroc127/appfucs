import { Injectable } from '@angular/core';
import {Observable,Subject} from 'rxjs'
import {UserDAO} from '../model/repositority/datasource/model.respository.datasource.UserDAO';

@Injectable({
  providedIn: 'root'
})
export class ServiceactiveusService {

  constructor(private daou:UserDAO) 
  {

  }



  public consultRols(token:string):Observable<any>
  {
  	return this.daou.consultRols(token);
  }


	public activAsigRol(usuarioactive:any,token:string):Observable<any>
	{
		return this.daou.activeRolUser(usuarioactive,token);
	}





}
