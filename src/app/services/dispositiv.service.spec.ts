import { TestBed } from '@angular/core/testing';

import { DispositivService } from './dispositiv.service';

describe('DispositivService', () => {
  let service: DispositivService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DispositivService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
