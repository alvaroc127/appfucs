import { Injectable } from '@angular/core';
import {PractionerDAO} from '../model/repositority/datasource/model.repository.datasource.PractioneDAO';
import {Observable} from 'rxjs';
import {AlertserviceService} from './alertservice.service';


@Injectable({
  providedIn: 'root'
})
export class AlertconfigService 
{

 

  constructor(private practidao:PractionerDAO,private alerser:AlertserviceService) 
  { 
  }


  public activeMovil(user:any,token:string):Observable<any>
  {
  	return this.practidao.activeMovil(user,token);
  }

  public activeAlert(user:any,token:string):Observable<any>
  {
  	return this.practidao.activeAlert(user,token);
  }

   public unactiveMovil(user:any,token:string):Observable<any>
  {
  	return this.practidao.unactveMovil(user,token);
  }

  public unactiveAlert(user:any,token:string):Observable<any>
  {
  	return this.practidao.unactiveAlert(user,token);
  }


  public isActiveAlert(user:any,token:string)
  {
      this.practidao.validateAlert(user,token).subscribe(
      response=>
      {
        if(response.status=='00')
        {
          this.alerser.subcribetop();
          
        }
      },
      error=>
      {
        console.log('no se pueden recibir alertas',error);
      }
      );
  }






}
