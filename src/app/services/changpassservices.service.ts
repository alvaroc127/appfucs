import { Injectable } from '@angular/core';
import {UserDAO} from '../model/repositority/datasource/model.respository.datasource.UserDAO';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangpassservicesService {

  constructor(private userDao:UserDAO) { 
  }



  public resetPass(usuario:any,token:string):Observable<any>
  {
  	return this.userDao.changePassword(usuario,token);
  }
}
