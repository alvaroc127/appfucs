import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {UserDAO} from '../model/repositority/datasource/model.respository.datasource.UserDAO';


@Injectable({
  providedIn: 'root'
})
export class UnactivateuserService 
{

  constructor(private userdao:UserDAO ) {

  }


 public unactivateUser(useractive:any,tokenhead:string):Observable<any>
  {
  	return this.userdao.unactiveRolUser(useractive,tokenhead);
  }
}
