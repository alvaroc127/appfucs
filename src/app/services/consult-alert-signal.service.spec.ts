import { TestBed } from '@angular/core/testing';

import { ConsultAlertSignalService } from './consult-alert-signal.service';

describe('ConsultAlertSignalService', () => {
  let service: ConsultAlertSignalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsultAlertSignalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
