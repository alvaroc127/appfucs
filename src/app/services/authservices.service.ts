import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { AuthFucs } from '../model/repositority/datasource/model.repository.datasource.auth';
import { AuthLogout } from '../model/repositority/datasource/model.repository.datasource.logout';
import {Rol } from '../model/rol.model';
import {Permiso } from '../model/permiso.model';
import {CifredServicesService} from '../util/cifred.services.service';
import {SessionUser,sessionUserInter} from '../model/sesion.model';
import {Usuario,InterfaceUsuario} from '../model/usuario.model';
import {JwtHelperService} from '@auth0/angular-jwt'
import {Subject,Subscription,Observable} from 'rxjs';
import {filter} from 'rxjs/operators';
import {AlertserviceService} from './alertservice.service';

@Injectable({
  providedIn: 'root'
})
export class AuthservicesService
{
  private username:string ='';

  private errrorMeessag:string= 'error in auth';

  public comunicate:Subject<string>;




  constructor
  (private authrest: AuthFucs,private cifredServi: CifredServicesService,
    private logout:AuthLogout,private alerser:AlertserviceService
    ) 
  { 
    this.comunicate=new Subject();

  }


  public authUser(username :string,password:string)
  {
  	let user=
  	{
  		'username':username,
  		'password':password
  	};

  	this.authrest.authUser(user).subscribe
  	(
  		response  => 
			{
				console.log('response que se recibe');
        //console.log(response);
				if(response.status=='00')
				{
					let roles=response.roles.map(
						 function(rol:any)
						{
              let permisos=new Array();
      						for (let i=0;i<permisos.length;i++)
      						{
                    let permi=permisos[i];
      							let permiso=new Permiso(permi[i].id,permi[i].descrip);
                    permisos.push(permiso);
      						}

                   if(rol.id == 4)
                {
                  console.log('cambio realizado')
                  rol.id=6;
                }

                let rol1=new Rol(
                  {permiso:permisos,id:rol.id,descrtip:rol.descrip
                });
                return rol1;
						}
          );

          let roletorder=roles.sort((rol1:Rol,rol2:Rol)=> 
          {
            if(rol1.id > rol2.id)
            {
              return 1;
            }
            if(rol1.id < rol2.id)
            {
              return -1;
            }
            return 0;
          });


          let sessi=new SessionUser
          (
             {
               token:response.accesToken,
                user: new Usuario
                 (
                   {
                  arraySingal:[],
                  rol:roletorder[0],
                  name:'',
                  numerodoc:'',
                  tipodoc:{id:'',descrip:''},
                  mail:'',
                  username:''}
                   ),
              expdate:response.expire,
              startDate:new Date()
             });
          sessi=this.decodeToken(sessi);
          sessi.expdate=response.timexpirMin;
          this.storeSessionUser(sessi);
          this.comunicate.next('sesionstart');
				}
			},
			error =>
			{
        this.comunicate.next('failsesion');
        this.errrorMeessag='auth error in comunication';
        console.log('errro ',error);
        console.log(this.errrorMeessag);
			}
  	);
  }






  private decodeToken(sesion: SessionUser): SessionUser
  {
    let jwtHelp= new JwtHelperService();
    const decodto=jwtHelp.decodeToken(sesion.token);
    sesion.user.username = decodto.preferred_username;
    this.username=decodto.preferred_username;
    return sesion;
  }


  public storeSessionUser(value: SessionUser)
  {
    if(this.username.length >0 && this.username !== '')
    {
        let strsesion=JSON.stringify(value);
        let encryp=this.cifredServi.encryptText(strsesion);
        sessionStorage.setItem('user',this.username);
        sessionStorage.setItem(this.username,encryp);
    }
  }

  public isStartSesion():Observable<string>
  {
    //filter((str:string)=>str=='sesionstart')
   return this.comunicate.pipe();
  }


  public getUserSession():any
  {
    this.username=String(sessionStorage.getItem('user'));
    let chryptex=sessionStorage.getItem(this.username);
    if(chryptex == null || chryptex == '' 
     || this.username == null || this.username =='')
    {
      throw -1;//no se encuentra la sesion
    }
    let tokenstr=this.cifredServi.decrypt(String(chryptex));
    let sesionUser=JSON.parse(tokenstr);
    let jwtHelp= new JwtHelperService();
    jwtHelp.decodeToken(sesionUser.token);
    //validar el con el tiempo de vida del toekn
    /*if(jwtHelp.isTokenExpired(sesionUser.gettoken))
    {
      throw -3;// la sesion expiro
    }*/
    return sesionUser;
  }

  //preguntar cual es el rol del usurio
  public isAdmin():boolean
  {
    let usese=this.getUserSession();
    let usu=usese.sesi.user.usu;
    return usu.rol.int.descrtip=="ADMINISTRADOR";
  }

  public isDoctorNurse():boolean
  {
     let usese=this.getUserSession();
     let usu=usese.sesi.user.usu; 
     return usu.rol.int.descrtip=="MEDICO" || usu.rol.int.descrtip == "ENFERMERA";
  }


  public isSesionOn():boolean
  {
    let usese=this.getUserSession();
    let startDat=new Date(usese.sesi.startDate);
    let horexp=usese.sesi.expdate;
    let active=false;
    let bandtokeHora=false;
    let bandtokenMin=true;
    if(horexp > 60)
     {
       bandtokenMin=false;
       bandtokeHora=true;
       horexp=horexp/60; 
     }


    let now=new Date();
    if(now.getFullYear() == startDat.getFullYear() &&
      now.getMonth() == startDat.getMonth() && 
      now.getDay() == startDat.getDay())
    {
      if(now.getHours()-startDat.getHours()>=23)
      {
        active=false;
      }else
      {
        if(bandtokeHora)
        {
          if(now.getHours()-startDat.getHours()<horexp)
          {
            active=true;
          }
        }
        if(bandtokenMin)
        {
          if(now.getMinutes()-startDat.getMinutes()<horexp+1)
          {
            active=true;
          }
        }
      }
    }
    return active;
  }



  public logoutUser()
  {
     let usese=this.getUserSession();
     let usu=usese.sesi.user.usu;
     let user=
    {
      username:usu.username
    }
    //console.log('el usuario va de salida ', usu);
    //console.log(user);
    this.logout.unloginUser(user).subscribe(
      subscribe=>
      {
        this.alerser.unsubscribtop();
        console.log('todo salio bien',subscribe);
      },
      error=>{
        console.log('ocurrio un erro',error);
      }

      );
  }


  public getUsername():string
  {
     let usese=this.getUserSession();
     let usu=usese.sesi.user.usu;
     return usu.username;
  }

  public getBarerToken():string
  {
    let usese=this.getUserSession();
    let token=usese.sesi.token;
    return token;
  }






}
