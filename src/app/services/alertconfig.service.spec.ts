import { TestBed } from '@angular/core/testing';

import { AlertconfigService } from './alertconfig.service';

describe('AlertconfigService', () => {
  let service: AlertconfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlertconfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
