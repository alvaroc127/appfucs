import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {PacienteDAO} from '../model/repositority/datasource/model.repository.datasource.PacienteDAO';

@Injectable({
  providedIn: 'root'
})
export class MonitorconfigService 
{
  constructor(private pacidao:PacienteDAO) 
  { 

  }

  public findUserNoMonitor(token:string):Observable<any>
  {
  	return this.pacidao.findPatients(token);
  }

  public findMonitorNoBind(token:string):Observable<any>
  {
  	return this.pacidao.findMonitors(token);
  }



  public bindUserMont(userbindmon:any,token:string):Observable<any>
  {
  	return this.pacidao.bindUserMont(userbindmon,token);
  }


  public findUserMont(token:string):Observable<any>
  {
  	return this.pacidao.findUserBindMont(token);
  }


  public unBindUserMont(userunbindmon:any,token:string):Observable<any>
  {
  	return this.pacidao.unBindUserMon(userunbindmon,token);
  }


}
