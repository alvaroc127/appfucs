import { NgModule} from  '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms'
import {navbar} from  './dashboard.navbar.component';
import {modelmodule} from '../model/model.module';



@NgModule({
	imports:[modelmodule,BrowserModule,FormsModule],
	declarations:[navbar],
	exports:[navbar]
})
export class navBarModule
{


}