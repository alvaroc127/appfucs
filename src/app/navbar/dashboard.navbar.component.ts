import {Component,Input} from '@angular/core';
import {modelmodule} from '../model/model.module';
import {AuthservicesService} from '../services/authservices.service';

@Component
({
	selector:'navbar',
	templateUrl:'./dashboard.navbar.component.html',
	styleUrls: ['./dashboard.navbar.component.css']
})
export class navbar {

    @Input() source: number=0;

    @Input() username: string='';

    constructor(private authserv:AuthservicesService)
    {


       

    }

    public url=
    {
          admin: '/admindashboard',
          medical:'/medicaldashboard'
    };
    public navarTitlesMedical=
        [ 
            {title: 'Configuración Alertas', uri:'/configalert'} ,
            {title: 'Configuración de Usuario', uri:'/configusu'} ,
            {title: 'Consulta Personalizada', uri:'/consultper'},
            {title: 'Desvicular Usuario', uri:'/devincularusumo'} 
        ];
     public navarTitlesAdmin=
     [
         {title: 'Registrar Usuario', uri:'/registrauser'},
         {title: 'Cambiar Password', uri:'/recoverpass'},
         {title: 'Activar Usuario', uri:'/adcionarrol'},
         {title: 'Desactivar Usuario', uri:'/quitarrol'}
     ] 



  title = 'frontFucs';
  isCollapse = false;   // guardamos el valor
toggleState() { // manejador del evento
        let foo = this.isCollapse;
        this.isCollapse = foo === false ? true : false; 
    }


getNavar()
    {
    	// esta lista retorna un objetio con dos atributos titulo y url
    	switch (this.source) {
            case 2:
                return this.navarTitlesAdmin;
             break;

             default:
                return this.navarTitlesMedical;
             break;
        }
    }

    getUrldashbor():string
    {
        switch (this.source) 
        {
            case 2:
                return this.url.admin;
            break;
            default:
                return this.url.medical;
             break;
        }

    }

 

}