import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { AuthservicesService} from '../../../services/authservices.service';
import {ChangpassservicesService} from '../../../services/changpassservices.service'

@Component({
  selector: 'app-recuperapassword',
  templateUrl: './recuperapassword.component.html',
  styleUrls: ['./recuperapassword.component.css']
})
export class RecuperapasswordComponent implements OnInit 
{
	public  sourchedash:number =2;

  constructor(private auth1:AuthservicesService, private changpass:ChangpassservicesService) 
  {

  }


   user=
   {
     name:'',
     email:'',
     documento:'',
     fechaNacimiento:'',
     tipodocumento:'',
     password:'',
     repipassword:''
   };

  public getUsername():string 
  {
     try
     {
       this.auth1.getUserSession();
       return this.auth1.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }else
       {
         sessionStorage.clear();
       }
       return String(e);
     }
  }

  ngOnInit(): void 
  {
  }


  public sendResetPassWord(user:any)
  {
    let token=this.auth1.getBarerToken();
     if(!this.auth1.isSesionOn())
      {
      alert('sesion off');
      this.auth1.logoutUser();
      sessionStorage.clear();
      return ;
      }
    this.changpass.resetPass(user,token).subscribe(
        subscribe=>
        {
          if(subscribe.status=='00')
          {
            alert('El password fue modificado de forma correcta');
          }
        },
        error=>
        {
          alert('no se pudo modificar el password el usuario');
          console.log('algo salio mal',error);
        }
      );
    this.resetForm();
  }

  public resetForm()
  {
     this.user.name='';
    this.user.email='';
    this.user.documento='';
    this.user.tipodocumento='';
    this.user.password='';
    this.user.repipassword='';
    this.user.fechaNacimiento=''
  }

}
