import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResgistrauserComponent } from './resgistrauser.component';

describe('ResgistrauserComponent', () => {
  let component: ResgistrauserComponent;
  let fixture: ComponentFixture<ResgistrauserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResgistrauserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResgistrauserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
