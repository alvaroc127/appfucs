import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { AuthservicesService} from '../../../services/authservices.service';
import {RegistrauserService} from '../../../services/registrauser.service';

@Component({
  selector: 'app-resgistrauser',
  templateUrl: './resgistrauser.component.html',
  styleUrls: ['./resgistrauser.component.css']
})
export class ResgistrauserComponent implements OnInit {

	public sourchedash:number =2;
  constructor(private auth1:AuthservicesService,private reserv:RegistrauserService) 
  {
   }

   user=
   {
     name:'',
     email:'',
     documento:'',
     fechaNacimiento:'',
     tipodocumento:'',
     password:'',
     repipassword:''
   };

  public getUsername():string 
  {
  	 try
     {
       this.auth1.getUserSession();
       return this.auth1.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }else
       {
         sessionStorage.clear();
       }
       return e;
     }
  }

  ngOnInit(): void {
  }




  public enviarForm(user: any)
  {
    if(this.user.name == null || this.user.email == null 
      || this.user.password  == null
      || this.user.repipassword == null
      || this.user.documento == null
      || this.user.tipodocumento == null 
      )
    {
      this.resetUser();
      alert('Faltan campos ');
    }
   
    if(!this.auth1.isSesionOn())
    {
      alert('sesion off');
      this.auth1.logoutUser();
      sessionStorage.clear();
      return ;
    }
    let tokent=this.auth1.getBarerToken();
    this.reserv.registraUser(user,tokent);
    this.resetUser();
  }


  public resetUser()
  {
    this.user.name='';
    this.user.email='';
    this.user.documento='';
    this.user.tipodocumento='';
    this.user.password='';
    this.user.repipassword='';
    this.user.fechaNacimiento=''
  }

}
