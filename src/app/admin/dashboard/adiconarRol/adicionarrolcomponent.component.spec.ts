import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarrolcomponentComponent } from './adicionarrolcomponent.component';

describe('AdicionarrolcomponentComponent', () => {
  let component: AdicionarrolcomponentComponent;
  let fixture: ComponentFixture<AdicionarrolcomponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarrolcomponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarrolcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
