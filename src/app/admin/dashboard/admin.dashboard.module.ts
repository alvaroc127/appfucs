import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {modelmodule} from '../../model/model.module';
import {navBarModule} from '../../navbar/dashboard.navbar.module';
import { RouterModule, Routes } from '@angular/router';
import {RoutingGuardAdmin} from '../../../routing.guard';
import {AdminDashboardComponent} from './admin.dashboard.component';
import {ResgistrauserComponent} from './resgistrauser/resgistrauser.component';
import {RecuperapasswordComponent} from './recuperapassword/recuperapassword.component';
import { AdicionarrolcomponentComponent } from './adiconarRol/adicionarrolcomponent.component';
import { QuitarRolComponent } from './quitarRol/quitar-rol.component';
import {AuthservicesService} from '../../services/authservices.service';





const routes:Routes =
[
	{path:"admindashboard", component:AdminDashboardComponent,
	canActivate:[RoutingGuardAdmin]},
	{path:"registrauser", component:ResgistrauserComponent,
	canActivate:[RoutingGuardAdmin]},
	{path:"recoverpass", component:RecuperapasswordComponent,
	canActivate:[RoutingGuardAdmin]},
	{path:"adcionarrol",component:AdicionarrolcomponentComponent,
	canActivate:[RoutingGuardAdmin]},
	{path:"quitarrol",component:QuitarRolComponent,
	canActivate:[RoutingGuardAdmin]}
];

@NgModule({
	imports:[navBarModule,modelmodule,BrowserModule,FormsModule,RouterModule.forRoot(routes)],
	declarations:[AdminDashboardComponent, ResgistrauserComponent,RecuperapasswordComponent, AdicionarrolcomponentComponent, QuitarRolComponent],
	providers:[RoutingGuardAdmin],
	exports:[RouterModule,AdminDashboardComponent]
})
export class AdminDashBoardModule{


}