import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {Usuario} from  '../../../model/usuario.model';
import {Permiso} from  '../../../model/permiso.model';
import {Rol} from  '../../../model/rol.model';
import { AuthservicesService} from '../../../services/authservices.service';
import {UnactivateuserService} from '../../../services/unactivateuser.service';



@Component({
  selector: 'app-quitar-rol',
  templateUrl: './quitar-rol.component.html',
  styleUrls: ['./quitar-rol.component.css']
})
export class QuitarRolComponent implements OnInit
{
 public sourchedash:number = 2;

// public userselect:Usuario;

 public quitarrol=
 {
   username:''
 }

 userform=
  {
    tipodoc:'',
    numerodoc:''
  }


  constructor(private auth:AuthservicesService, private rout:Router,
    private unactser:UnactivateuserService) 
  {
  	/*let per =new Permiso(1,'Permiso test');
  	let permi=new Array();
  	permi.push(per);
  	let rol=new Rol
    ({
      permiso:permi,
      id:1,
      descrtip:'Rol test'
      }
     );
  	 this.userselect= new Usuario({ 
         arraySingal:new Array(),
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
  	 this.userselect.username='prueba nobmre';*/
  }

  public getUsername():string 
  {
  	  try
     {
       this.auth.getUserSession();
       return this.auth.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
         this.rout.navigate(['/login']);
       }else
       {
         this.auth.logoutUser();
         sessionStorage.clear();
         this.rout.navigate(['/login']);
       }
       return String(e);
     }
  }



  public unactiveUser(useform:any)
  {
     if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
     }

    try
    {
      this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    let token=this.auth.getBarerToken();
    this.quitarrol.username=useform.tipodoc+useform.numerodoc;
    this.unactser.unactivateUser(this.quitarrol,token).subscribe(
      response=>
      {
        if(response.status=='00')
        {
          alert('se quito el rol y desactivo el usuario');
        }
      },
      error=>
      {
        console.log('error en quitar rol',error);
        alert('no sepudo desvincular usuario');
      }

      );
    this.resetObj();
  }

  public resetObj()
  {
    this.quitarrol.username='';
    this.userform.numerodoc='';
    this.userform.tipodoc='';
  }

  /*public getUserWithRol(): Usuario[]
  {
  	let per =new Permiso(1,'Permiso test');
  	let permi=new Array();
  	permi.push(per);
  	let rol=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol test'
      });

    
  	 let uss= new Usuario(
       { 
         arraySingal:new Array(),
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         }
       );
  	 uss.name='prueba nobmre';
  	 uss.numerodoc='1010230671';
  	 uss.username='11010230671';
     uss.tipodoc=this.getTypeDoc(this.userselect.username.charAt(0)); 
  	 let arr =new Array();


    let rol1=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol test2'
      });
  	 let uss1= new Usuario({ 
         arraySingal:new Array(),
         rol:rol1,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
  	 uss1.name='prueba 2 ';
  	 uss1.numerodoc='51986066';
  	 uss1.username='251986066';
     uss1.tipodoc=this.getTypeDoc(this.userselect.username.charAt(0)); 
  	 arr.push(uss);
  	 arr.push(uss1);
  	 return arr;
  }*/


 /* public setUserSelect()
  {
     this.userselect=this.getUserWithRol().filter
     ( user => 
     {
       return user.username == this.userselect.username;
     })[0];
     //this.userselect.setTipoDoc();
     this.userselect.tipodoc=this.getTypeDoc(this.userselect.username.charAt(0)); 
  }*/

   /*public getTypeDoc(tipdoc:string):any
  {
    let array=
    [
      { id: '1', descrip:'cedula' },
      { id: '2', descrip:'cedula extranjeria' },
      { id: '3', descrip:'tarjeta identidad' }
    ]
    return  array.filter( (tip:any) => { return tip.id == tipdoc})[0];
  }*/


  public getTypeDoc():any
  {
    let array=
    [
      { id: '1', descrip:'cédula' },
      { id: '2', descrip:'cédula extranjería' },
      { id: '3', descrip:'tarjeta identidad' }
    ]
    return  array;
  }

  ngOnInit(): void {
  }

}
