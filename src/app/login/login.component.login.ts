import {Component,OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {modelmodule} from '../model/model.module';
import {AuthservicesService} from '../services/authservices.service';
import {AlertconfigService} from '../services/alertconfig.service';



@Component({
	selector: 'login',
	templateUrl: './login.Component.html',
	styleUrls: ['login.Component.css']
})




export class login implements OnInit
{

	public error:string='';
	public ngOnInit()
	{
		this.auth.isStartSesion().subscribe
		(
			subsribe=>
			{
				try
				{
					if(subsribe == 'failsesion')
					{
						this.error='ocurrio un error iniciando sesion';
						alert(this.error);
					}
					else
					{
				 		this.auth.getUserSession();
				 		if(this.auth.isAdmin())
				 		{
				 			this.route.navigate(['/admindashboard']);
				 		}
				 		else
				 	    {
				 		if(this.auth.isDoctorNurse())
				 		{
				 			this.useralert.username=this.auth.getUsername();
				 			
				 			console.log(this.useralert);
				 			let token=this.auth.getBarerToken();
				 			this.alersercf.isActiveAlert(this.useralert,token);
				 			this.route.navigate(['medicaldashboard']);
				 		}
				 	  }
				 		
					}
				}catch(e)
				{
					console.log("ocurrio un error");
					console.log(e);
				}
			},
			error=>
			{

			}
		)

	}


	formuser=
	{
		numerodoc:'',
		tipodoc:'',
		username:'',
		password:''
	}
	useralert=
	{
		username:''

	}

	


	resetvar()
	{
		this.formuser.numerodoc='';
		this.formuser.tipodoc='';
		this.formuser.password='';
		this.formuser.username='';
	}


	constructor(private auth:AuthservicesService,private route:Router,
		private alersercf:AlertconfigService)
	{


	}


	public getTipoDocumento():any 
	{

		 let array=
    [
      { id: '1', descrip:'cédula' },
      { id: '2', descrip:'cédula extranjería' },
      { id: '3', descrip:'tarjeta identidad' }
    ]
    	return array;
	}

	public onsubimitform(form:any)
	{
		form.username= form.tipodoc+form.numerodoc;
		if(form.tipodoc !=='' && form.numerodoc !== '' && form.password !== '')
		{
		this.auth.authUser(form.username,form.password);
		this.resetvar();
		}
	}

}