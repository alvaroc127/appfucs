import { TestBed } from '@angular/core/testing';

import { CifredServicesService } from './cifred.services.service';

describe('CifredServicesService', () => {
  let service: CifredServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CifredServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});


describe('testcifred',()=> {
	let servic :CifredServicesService;
 	beforeEach(() => {
    TestBed.configureTestingModule({});
    servic = TestBed.inject(CifredServicesService);
  	});

	it('un cifre chain',()=>{
		let out=servic.encryptText('micidremessage');
		let flag=out == '';
		console.log(out);
		expect(flag).toBe(false);
	});
});


describe('testdecrypt',()=> {
	let servic :CifredServicesService;
 	beforeEach(() => {
    TestBed.configureTestingModule({});
    servic = TestBed.inject(CifredServicesService);
  	});

	it('un cifre chain',()=>{
		let out=servic.decrypt('U2FsdGVkX18zUg0lI57lI9bOXAP9d6+CJbR+ALOiS5Y=');
		let flag=out == 'micidremessage';
		console.log(out+'___BANDERA___'+flag);
		expect(flag).toBe(true);
	});
});
