import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CifredServicesService 
{

  constructor() { }


  public encryptText(messg :string):string
  {
  	let pass=environment.cifredkey;
  	let cifredtext=CryptoJS.AES.encrypt(messg,pass);

	//let outiv1=iv1.toString(CryptoJS.enc.Base64);

	//let key=CryptoJS.enc.Utf8.stringify(cifredtext.key) //cifredtext.key.toString(CryptoJS.enc.Utf8);
  	//let outcifred=cifredtext.toString(CryptoJS.enc.Utf8);
  	//let outiv1=cifredtext.iv.toString(CryptoJS.enc.Base64);
  	//console.log(key);
  	//console.log(cifredtext.toString());
  	//let out=`${outcifred}FFF_FFF_FFF${outiv1}`;
  	return cifredtext.toString();
  }



  public decrypt(str:string):string
  {
  	//let vect=str.split(/F{3}_F{3}_F{3}/);
  	//let cifredst=vect[0];
  	//let iv1st=vect[1];
  	//let pass=CryptoJS.enc.Utf8.parse(environment.cifredkey);
  	//console.log(str);
  	let pass=environment.cifredkey;
  	//console.log(pass);


  	//let cifredarr=CryptoJS.enc.Utf8.parse(cifredst);
  	let cif=CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(str));
  	//let cifst=CryptoJS.enc.Utf8.stringify(cif);
  	//console.log(CryptoJS.enc.Utf8.parse(str));

 	//let cifrest=CryptoJS.enc.Utf8.stringify(cifredarr);
  	let dec=CryptoJS.AES.decrypt(str,pass);
  	//console.log('decrypt__'+ CryptoJS.enc.Utf8.stringify(dec));
  	return CryptoJS.enc.Utf8.stringify(dec);
  }






}
