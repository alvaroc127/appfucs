import { ECG} from './ECG.model';

export class Alertas extends ECG
{

	private horaalert: string;

	private severidad:string;

	private descripcion:string;

	private id:string;

	constructor()
	{
		super();
		this.horaalert='';
		this.severidad='';
		this.descripcion='';
		this.id='';
	}


	 get gethoraalert()
	{
		return this.horaalert;
	}

	get getseveridad()
	{
		return this.severidad;
	}

	get getdescripcion()
	{
		return this.descripcion;
	}

	get getid()
	{
		return this.id;
	}  

	set setid(id1:string)
	{
		this.id=id1;
	}

	set sethoraalert(horaaler:string)
	{
		this.horaalert=horaaler;
	}


	set setseveridad(severid:string)
	{
		this.severidad=severid;
	}



	set setdescripcion(descrip:string)
	{
		this.descripcion=descrip;
	}


}