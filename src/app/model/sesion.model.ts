import { Usuario} from './usuario.model';

export interface sessionUserInter
{
 	 token : string;
	 user: Usuario;
	 expdate: number;
	 startDate: Date;
}


export class SessionUser implements sessionUserInter
{


	constructor (public sesi:sessionUserInter)
	{
		
	}


	get token():string
	{
		return this.sesi.token;
	}


	set token(token1:string)
	{
		this.sesi.token=token1;
	}

	get user(): Usuario
	{
		return this.sesi.user;
	}

	set user(usuario1:Usuario)
	{
		this.sesi.user=usuario1;
	}

	get expdate():number
	{
		return this.sesi.expdate;
	}

	set expdate(exp1:number)
	{
		this.sesi.expdate=exp1;
	}

	get startDate():Date
	{
		return this.sesi.startDate;
	}

	set startDate(datstart:Date)
	{
		this.sesi.startDate=datstart;
	}

}