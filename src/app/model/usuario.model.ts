import {Signal}  from './signal.model';
import {Rol} from './rol.model';

export interface InterfaceUsuario {
    arraySingal: Signal[];
    rol: Rol;
    name: string;
    numerodoc:string;
    tipodoc:any;
    mail: string;
    username:string;
}


export class Usuario implements InterfaceUsuario
{
 

 	constructor(public  usu:InterfaceUsuario)
 	{
 		//this.arraySingal=usu.arraySingal;
 		//this.rol=usu.rol;
 	}

 	get rol():Rol
 	{
 		return this.usu.rol;
 	}

 	set rol(rol1:Rol)
 	{
 		this.usu.rol=rol1;
 	}

 	


 	get arraySingal():Signal[]
 	{
 		return this.usu.arraySingal;
 	}

 	set arraySingal(sigarr:Signal[])
 	{
 		this.usu.arraySingal=sigarr;
 	}



 	get name():string 
 	{
 		return this.usu.name;
 	}

 	set name(nam:string)
 	{
 		this.usu.name=nam;
 	}


 	get username():string
 	{
 		return this.usu.username;
 	}

 	set username(use:string)
 	{
 		this.usu.username=use;
 	}


 	get numerodoc():string
 	{
 		return this.usu.numerodoc;
 	}

 	set numerodoc(nund:string)
 	{
 		this.usu.numerodoc=nund;
 	}


 	get tipodoc():any
 	{
 		return this.usu.tipodoc;
 	}

 	set tipodoc(tip:any)
 	{
 		this.usu.tipodoc=tip;
 	}


 	get mail():string
 	{
 		return this.usu.mail;
 	}

 	set mail(maill:string)
 	{
 		this.usu.mail=maill;
 	}


}