export class Monitor
{
	protected ip :string;

	protected id: number;

	constructor( ip1:string, id1 :number) 
	{
		this.ip=ip1;
		this.id=id1;
	}


	get getip():string
	{
		return this.ip;
	}

	set setip(ip1:string)
	{
		this.ip=ip1;
	}


	get getid():number
	{
		return this.id;
	}

	set setid(id1:number)
	{
		this.id=id1;
	}

}