import {SessionUser} from '../../sesion.model';
import {Injectable} from '@angular/core'
import {Observable, from } from 'rxjs'
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {environment} from '../../../../environments/environment';


@Injectable()
export class AuthLogout
{

	url:string;

	constructor(private http:HttpClient)
	{
		this.url=environment.baseurl+environment.logout;
	}



	public unloginUser(usuario:any):Observable<any>
	{
		let httphead=new HttpHeaders
		({
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});	
		return this.http.post(this.url,usuario,{'headers':httphead});
	}





}