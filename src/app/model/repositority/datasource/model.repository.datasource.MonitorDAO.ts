import {Injectable} from '@angular/core'
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Monitor} from '../../monitor.model';
import { Observable } from "rxjs";
@Injectable()
export class MonitorDAO
{

	private monitores:Monitor[]= [];

	constructor(private http:HttpClient, private url:string)
	{

	}


	public findMonitorNoBind(body: any): Observable<any>
	{
		return this.http.post<any>(this.url,body);
	}

	public monitorNoBind(body:any)
	{
		this.findMonitorNoBind(body).subscribe
		(
			response  => 
			{
				console.log('se reciben los monitores',response);
				for (const key of response.map.keys()) 
				{
					let mon=new Monitor(key,response.map.get(key));
					this.monitores.push(mon);
				}
			},
			error =>
			{
				console.log('ERROR ', error);

			}
		)
	}

	public get getMonitores():Monitor[]
	{
		return this.monitores;
	}


}



