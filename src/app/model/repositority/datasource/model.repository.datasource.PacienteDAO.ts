import {Injectable} from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';
import {Observable, from } from 'rxjs';
import { Patient } from '../../paciente.model';
import {Rol} from '../../rol.model';
import {Permiso} from '../../permiso.model';
import {environment} from '../../../../environments/environment'

@Injectable()
export class PacienteDAO
{

	private url:string;
	constructor(private httpcli:HttpClient)
	{
		this.url='';

	}
	public findPatients(token:string): Observable<any> 
	{
		this.url=environment.baseurl+environment.medicalendpoints.consultusernomon;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,{},{'headers':httphead});
	}

	public findMonitors(token:string):Observable<any>
	{
		this.url=environment.baseurl+environment.medicalendpoints.findmon;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,{},{'headers':httphead});
	}


	public bindUserMont(user:any,token:string):Observable<any>
	{
		this.url=environment.baseurl+environment.medicalendpoints.bindusermon;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,user,{'headers':httphead});
	}


	public findUserBindMont(token:string):Observable<any>
	{
		this.url=environment.baseurl+environment.medicalendpoints.consultusermon;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,{},{'headers':httphead});
	}


	public unBindUserMon(userunbind:any,token:string):Observable<any>
	{
		this.url=environment.baseurl+environment.medicalendpoints.unbindusermon;
		let httphead=new HttpHeaders({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,userunbind,{'headers':httphead});
	}

	/*public findPatient(): Patient 
	{
		let per=new Permiso(1,'otropermiso');
		let arrper=new Array();
		arrper.push(per);
		let rol =new Rol({
      permiso:arrper,
      id:1,
      descrtip:'Rol test2'
      });



		return new Patient({ 
         arraySingal:[],
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
	}*/



}
