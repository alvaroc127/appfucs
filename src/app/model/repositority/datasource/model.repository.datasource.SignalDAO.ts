import {Injectable} from '@angular/core'
import {Observable, from } from 'rxjs'
import {HttpClient,HttpHeaders}from '@angular/common/http';
import {Signal} from '../../signal.model';
import {Usuario} from '../../usuario.model';
import {environment} from '../../../../environments/environment'

@Injectable()
export class SignalDAO
{

	private url:string;
    constructor(private http:HttpClient)
    {
    	this.url='';

    }

	public  findAlertsbyPatient(user:any, token:string):Observable<any>
	{
		this.url=environment.baseurl+environment.medicalendpoints.consultalert;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.http.post<any>(this.url,user,{'headers':httphead});
	}


	public findSignalLastHour(user: any, token: string):Observable<any>
	{
		this.url=environment.baseurl+environment.medicalendpoints.consultsignal;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.http.post<any>(this.url,user,{'headers':httphead});
	}














}