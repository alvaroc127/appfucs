import {Permiso}  from './permiso.model';

export interface interRol
{
	permiso: Permiso[];
	id: number;
    descrtip: string;

}

export class Rol implements interRol
{

	constructor(public int:interRol)
	{
		
	}


	get permiso(): Permiso[]
	{
		return this.int.permiso;
	}


	set permiso(permis1: Permiso[])
	{
		this.int.permiso=permis1;
	}

	get descrtip(): string
	{
		return this.int.descrtip;
	}

	set descrtip(destri:string)
	{
		this.int.descrtip=destri;
	}


	get  id():number
	{
		return this.int.id;
	}
	set id(nid:number)
	{
		this.int.id=nid;
	}
}