export interface permisointer
{
	iden: number;
	describ: string;
}

export class Permiso
{

	protected iden: number;

	protected describ: string;

	constructor(iden1 :number,describ1: string)
	{
		this.iden=iden1;
		this.describ=describ1;
	}


	get getdescrib():string
	{
		return this.describ;
	}

	set setdescrib( describ1:string)
	{
		this.describ=describ1;
	}


	get getiden():number
	{
		return this.iden;
	}


	set setiden(number1: number)
	{
		this.iden=number1;
	}


}