import { Injectable } from '@angular/core';
import { Router,CanActivate, CanActivateChild, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthservicesService} from './app/services/authservices.service';

@Injectable()
export class RoutingGuardAdmin implements CanActivate, CanActivateChild, CanDeactivate<unknown> {
  private firstNavigation = false;

  constructor(private auth:AuthservicesService,private route:Router)
  {


  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
  {
    let isinsesion=false;
    try
    {
      this.auth.getUserSession();
      if(this.auth.isAdmin() && this.auth.isSesionOn())
      {
        isinsesion=true;
      } else
      {
        alert(" su sesion vencio o invalid");
        this.auth.logoutUser();
        sessionStorage.clear();
        this.route.navigate(['/login']);
      }      
    }catch(e)
    {
      console.log('inicio consulta 22');
      console.log(e);
      if(e == -1)
      {
        isinsesion=false;
        alert(" necesita tener una sesion activa para acceder");
        //this.auth.logoutUser();
        this.route.navigate(['/login']);
        //this.logout.logoutUser();
        sessionStorage.clear();
      }

    }
    return isinsesion;
  }
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
  {
   return false;
  }
  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return false;
  }
  
}
